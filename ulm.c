#include <stdio.h>
#include <stdlib.h>

#include "alu.h"
#include "cu.h"
#include "loader.h"
#include "vmem.h"

int
main(int argc, char **argv)
{
    if (argc != 2) {
	fprintf(stderr, "usage: %s prog\n", argv[0]);
	exit(1);
    }
    load(argv[1]);

    run();
}
