#include <stdlib.h>
#include <stdio.h>

#include "cu.h"
#include "vmem.h"

enum {
    PAGE_BITS = 16,
    PAGE_SIZE = 1 << PAGE_BITS,
    SET_BITS = 8,
    SET_SIZE = 1 << SET_BITS,
};

enum {
    BYTE = 1,
    WORD = 2,
    LONG = 4,
    QUAD = 8
};

static uint64_t signMask[QUAD + 1] = {
    0,			// size = 0
    0x80,		// size = 1
    0x8000,		// size = 2
    0,			// size = 3
    0x80000000,		// size = 4
    0,			// size = 5
    0,			// size = 6
    0,			// size = 7
    0x8000000000000000,	// size = 8
};

static uint64_t signExtMask[QUAD + 1] = {
    0,			// size = 0
    0xFFFFFFFFFFFFFF00,	// size = 1
    0xFFFFFFFFFFFF0000,	// size = 2
    0,			// size = 3
    0xFFFFFFFF00000000,	// size = 4
    0,			// size = 5
    0,			// size = 6
    0,			// size = 7
    0,			// size = 8
};

struct Page
{
    uint64_t tag;
    uint8_t byte[PAGE_SIZE]; 
    struct Page *next;
};

struct Page *table[SET_SIZE];

static struct Page *
getPage(uint64_t addr, uint64_t *offset)
{
    *offset = addr & (PAGE_SIZE - 1);
    uint64_t set = (addr >> PAGE_BITS) & (SET_SIZE - 1);
    uint64_t tag = addr >> (PAGE_BITS + SET_BITS);

    for (struct Page *p = table[set]; p; p = p->next) {
	if (p->tag == tag) {
	    return p;
	}
    }

    // create a clean new memory page
    struct Page *p = calloc(1, sizeof(*p));
    
    if (! p) {
	fprintf(stderr, "internal error: out of memory\n");
	abort();
    }

    p->tag = tag;
    if (table[set]) {
	p->next = table[set];
    }
    table[set] = p;
    return p;
}

void
fetch32(uint64_t addr, uint32_t *val)
{
    if (! val) {
	fprintf(stderr, "internal error: 'fetch32' requires non zero register");
	abort();
    }
    if (addr % 4) {
	fprintf(stderr, "bad alignment: addr = 0x%016llx, size = 4\n", addr);
	printCU();
	abort();
    }
    uint64_t offset;
    struct Page *p = getPage(addr, &offset);

    *val = 0;
    for (size_t i = 0; i < 4; ++i) {
	*val = *val << 8 | p->byte[offset + i];
    }
}

void
fetch64(int8_t disp, Reg base, Reg index, size_t scale,
	enum Extension ext, size_t numBytes, Reg dest)
{
    if (! dest) {
	return;
    }

    uint64_t addr = disp + regVal(base) + scale * regVal(index);

    if (addr % numBytes) {
	fprintf(stderr, "bad alignment: addr = 0x%016llx, size = %zu\n",
		addr, numBytes);
	fprintf(stderr, "disp = %d, base = %d, regVal(base) = 0x%llx, "
		"index = %d, regVal(index) = 0x%llx\n", disp, base,
		regVal(base), index, regVal(index));
	printCU();
	abort();
    }

    uint64_t offset;
    struct Page *p = getPage(addr, &offset);
    uint64_t *val = regDevice(dest);

    *val = 0;
    for (size_t i = 0; i < numBytes; ++i) {
	*val = *val << 8 | p->byte[offset + i];
    }

    if (numBytes == QUAD || ext == ZERO_EXT || (*val & signMask[numBytes]) == 0)
    {
	return;
    }
    *val |= signExtMask[numBytes];
}

void
store64(int8_t disp, Reg base, Reg index, size_t scale,
	size_t numBytes, Reg src)
{
    uint64_t addr = disp + regVal(base) + scale * regVal(index);

    if (addr % numBytes) {
	fprintf(stderr, "bad alignment: addr = 0x%016llu, size = %zu\n",
		addr, numBytes);
	abort();
    }

    uint64_t offset;
    struct Page *p = getPage(addr, &offset);

    uint64_t data = src ? *regDevice(src) : 0;

    for (size_t i = 0; i < numBytes; ++i) {
	p->byte[offset + numBytes - i - 1] = data & 0xFF;
	data >>= 8;
    }
}

void
store8(uint64_t addr, uint8_t val)
{
    uint64_t offset;
    struct Page *p = getPage(addr, &offset);

    p->byte[offset] = val;
}


void
printVMemMapped()
{
    for (size_t set = 0; set < SET_SIZE; ++set) {
	printf("set 0x%zx:\n", set);
	for (struct Page *p = table[set]; p; p = p->next) {
	    printf("tag 0x%llx:\n", p->tag);
	    for (size_t offset = 0; offset < PAGE_SIZE; ++offset) {
		printf(" 0x%02x", (int ) p->byte[offset]);
	    }
	    printf("\n");
	}
    }
}

void
printVMem(uint64_t begin, uint64_t end, size_t chunkSize)
{
    for (uint64_t addr = begin; addr < end; addr += chunkSize) {
	printf("0x%016llx: ", addr);

	for (size_t i = 0; i < chunkSize; ++i) {
	    uint64_t offset;
	    struct Page *p = getPage(addr + i, &offset);

	    printf("%02x ", p->byte[offset]);
	}
	printf("\n");
    }
}
