#include <stddef.h>
#include <stdio.h>

#include "alu.h"
#include "cu.h"


uint64_t regDev[256];
bool statusReg[NUM_STATUS_FLAGS];

uint64_t *
regDevice(Reg reg)
{
    if (! reg) {
	return 0;
    }
    return &regDev[reg];
}

uint64_t
regVal(Reg reg)
{
    return regDev[reg];
}

void
setReg(uint64_t val, Reg reg)
{
    if (! reg) {
	return;
    }
    regDev[reg] = val;
}

// compute b + a
void
add64(uint64_t a, uint64_t b, Reg dest)
{
    uint64_t s = b + a;
    int64_t a_ = a, b_ = b, s_ = s;

    setReg(s, dest);
    statusReg[ZF] = s == 0;
    statusReg[CF] = s < a || s < b;
    statusReg[SF] = s_ < 0;
    statusReg[OF] = (a_ > 0 && b_ > 0 && s_ < 0)
	|| (a_ < 0 && b_ < 0 && s_ > 0);
}

// compute b - a
void
sub64(uint64_t a, uint64_t b, Reg dest)
{
    uint64_t s = b - a;
    int64_t a_ = a, b_ = b, s_ = s;

    //printf("sub64: %016llx - %016llx = %016llx", b, a, s);

    setReg(s, dest);
    statusReg[ZF] = s == 0;
    statusReg[CF] = s > b;
    statusReg[SF] = s_ < 0;
    statusReg[OF] = (a_ > 0 && b_ < 0 && s_ > 0)
	|| (a_ < 0 && b_ > 0 && s_ < 0);
}

// compute b * a
void
mul64(uint64_t a, uint64_t b, Reg dest)
{
    uint64_t s = b * a;
    setReg(s, dest);
}

void
mul128(uint64_t a, uint64_t b, Reg dest)
{
    uint64_t a0 = a & 0xFFFFFFFF;
    uint64_t a1 = a >> 32;
    uint64_t b0 = b & 0xFFFFFFFF;
    uint64_t b1 = b >> 32;

    uint64_t ab00 = a0 * b0;
    uint64_t ab01 = a0 * b1;
    uint64_t ab10 = a1 * b0;
    uint64_t ab11 = a1 * b1;

    uint64_t s0 = (ab00 >> 32) + (ab01 & 0xFFFFFFFF) + (ab10 & 0xFFFFFFFF);
    uint64_t s1 = (s0 >> 32) + (ab01 >> 32) + (ab10 >> 32) + ab11;
    s0 = (s0 << 32) + (ab00 & 0xFFFFFFFF);

    setReg(s0, dest);
    setReg(s1, dest + 1);
    statusReg[CF] = s1 != 0;
}

void
idiv64(int64_t a, int64_t b, Reg dest)
{
    int64_t sgnA = a < 0 ? -1 : 1;
    int64_t sgnB = b < 0 ? -1 : 1;

    int64_t abs_a = a < 0 ? -a : a;
    int64_t abs_b = b < 0 ? -b : b;

    int64_t q = sgnA*sgnB*(abs_b / abs_a);
    int64_t r = b - a * q;

    setReg(q, dest);
    setReg(r, dest + 1);
}

void
div128(uint64_t a, uint64_t b0, uint64_t b1, Reg dest)
{
    unsigned __int128 b = b1;
    b = b << 64 | b0;

    unsigned __int128 q = b / a;
    uint64_t r = b % a;

    setReg(q & 0xFFFFFFFFFFFFFFFF, dest);
    setReg((q >> 64) & 0xFFFFFFFFFFFFFFFF, dest + 1);
    setReg(r, dest + 2);
}

// compute b * 2^a mod 2^64
void
shiftLeft64(uint64_t a, uint64_t b, Reg dest)
{
    uint64_t s = b << a;
    setReg(s, dest);
    statusReg[CF] = (b >> (64 - a)) & 0x1;
}

// compute b / 2^a mod 2^64
void
shiftRightSigned64(uint64_t a, int64_t b, Reg dest)
{
    uint64_t s = b >> a;
    setReg(s, dest);
}

void
shiftRightUnsigned64(uint64_t a, uint64_t b, Reg dest)
{
    uint64_t s = b >> a;
    setReg(s, dest);
}

void
and64(uint64_t a, uint64_t b, Reg dest)
{
    uint64_t s = a & b;
    setReg(s, dest);
    statusReg[ZF] = s == 0;
}

void
or64(uint64_t a, uint64_t b, Reg dest)
{
    uint64_t s = a | b;
    setReg(s, dest);
    statusReg[ZF] = s == 0;
}

void
not64(uint64_t a, Reg dest)
{
    uint64_t s = ~a;
    setReg(s, dest);
    statusReg[ZF] = s == 0;
}

void
printALU(uint8_t firstReg, uint8_t lastReg)
{
    printf("ZF = %d, CF = %d, SF = %d, OF = %d\n", statusReg[ZF], statusReg[CF],
	   statusReg[SF], statusReg[OF]);
    for (size_t i = firstReg; i <= lastReg; ++i) {
	printf("%%%02zx: 0x%016llx\n", i, regDev[i]);
    }
}
