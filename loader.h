#ifndef ULM_LOADER_H
#define ULM_LOADER_H

extern uint64_t brk;

void load(const char *prog);

#endif // ULM_LOADER_H
