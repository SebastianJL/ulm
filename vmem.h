#ifndef ULM_VMEM_H
#define ULM_VMEM_H

#include <stddef.h>
#include <stdint.h>

#include "alu.h"

enum Extension {
    ZERO_EXT,
    SIGN_EXT,
};

// for fetching an instruction
void fetch32(uint64_t addr, uint32_t *val);

// for fetching data into an ALU register
void fetch64(int8_t disp, Reg base, Reg index, size_t scale, enum Extension ext,
	     size_t numBytes, Reg dest);

// for storing data from an ALU register
void store64(int8_t disp, Reg base, Reg index, size_t scale, size_t numBytes,
	     Reg src);

void store8(uint64_t addr, uint8_t val);

void printVMemMapped();
void printVMem(uint64_t begin, uint64_t end, size_t chunkSize);

#endif // ULM_VMEM_H
