#ifndef ULM_INSTRSET_H
#define ULM_INSTRSET_H

#include <stdint.h>

extern struct OpCode {
    void (*instr)();
    const char *(*makeAsmStr)();
} opCode[256];

#endif // ULM_INSTRSET_H
