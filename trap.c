#include <stdio.h>
#include <stdlib.h>

#include "trap.h"

void
trap()
{
    fprintf(stderr, "sorry, currently no trap is supported");
    abort();
}
