#include <stdio.h>
#include <stdlib.h>

#include "alu.h"
#include "cu.h"
#include "instrset.h"
#include "vmem.h"

union InstrReg instrReg;

static uint64_t instrPtr;
static bool jmp;

void
absJump(uint64_t addr,  Reg retReg)
{
    setReg(instrPtr + 4, retReg);
    instrPtr = addr;
    jmp = true;
}

void
relJump(uint64_t offset)
{
    if (offset >= 0x800000) {
	offset |= 0xFFFFFFFFFF000000;
    }
    instrPtr += offset << 2;
    jmp = true;
}

void
halt(int8_t code)
{
    exit(code);
}

void
incrInstrPtr()
{
    if (! jmp) {
	instrPtr += 4;
    }
    jmp = false;
}

void
decodeInstr()
{
    void (*instr)() = opCode[instrReg.Op].instr;

    if (! instr) {
	fprintf(stderr, "illegal instruction\n");
	printCU();
	abort();
    }
}

void
executeInstr()
{
    void (*instr)() = opCode[instrReg.Op].instr;

    if (! instr) {
	fprintf(stderr, "illegal instruction\n");
	printCU();
	abort();
    }
    instr();
    uint32_t tmp;
    fetch32(0x60, &tmp);
}

void
fetchInstr()
{
    fetch32(instrPtr, &instrReg.OpXYZ);
}

void
run()
{
    while (true) {
	fetchInstr();
	executeInstr();
	incrInstrPtr();
    }
}

void
printCU()
{
    printf("Op = %02x, X = %02x, Y = %02x, Z = %02x\n", (int) instrReg.Op,
	   (int) instrReg.X, (int) instrReg.Y, (int) instrReg.Z);
    printf("OpXYZ = %08x\n", (int) instrReg.OpXYZ);
    printf("%%IP = %llx\n", instrPtr);
}
