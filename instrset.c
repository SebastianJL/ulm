#include <stdio.h>

#include "alu.h"
#include "cu.h"
#include "io.h"
#include "instrset.h"
#include "trap.h"
#include "vmem.h"

//- cu

static void
nop()
{
}

static void
haltImm()
{
    halt(instrReg.X);
}

static void
haltReg()
{
    halt(regVal(instrReg.X) & 0xFF);
}

static void
ja()
{
    if (statusReg[CF] == 0 && statusReg[ZF] == 0) {
	relJump(instrReg.XYZ);
    }
}

static void
jae()
{
    if (statusReg[CF] == 0) {
	relJump(instrReg.XYZ);
    }
}

static void
jnae()
{
    if (statusReg[CF] == 1) {
	relJump(instrReg.XYZ);
    }
}

static void
jna()
{
    if (statusReg[CF] == 1 || statusReg[ZF] == 1) {
	relJump(instrReg.XYZ);
    }
}

static void
je()
{
    if (statusReg[ZF] == 1) {
	relJump(instrReg.XYZ);
    }
}

static void
jg()
{
    if (statusReg[ZF] == 0 && statusReg[OF] == statusReg[SF]) {
	relJump(instrReg.XYZ);
    }
}

static void
jge()
{
    if (statusReg[OF] == statusReg[SF]) {
	relJump(instrReg.XYZ);
    }
}

static void
jnge()
{
    if (statusReg[OF] != statusReg[SF]) {
	relJump(instrReg.XYZ);
    }
}

static void
jle()
{
    if (statusReg[ZF] == 1 || statusReg[OF] != statusReg[SF]) {
	relJump(instrReg.XYZ);
    }
}

static void
jne()
{
    if (statusReg[ZF] == 0) {
	relJump(instrReg.XYZ);
    }
}

static void
jmpReg()
{
    absJump(regVal(instrReg.X), instrReg.Y);
}

static void
jmpRel()
{
    relJump(instrReg.XYZ);
}

//- io
static void
putcImm()
{
    printChar(instrReg.X);
}

static void
putcReg()
{
    printChar(regVal(instrReg.X));
}

static void
getcReg()
{
    setReg(0xFF & readChar(), instrReg.X);
}

//- alu

static void
imulqImm()
{
    uint64_t X = instrReg.X;
    if (X > 0x7F) {
	X |= 0xFFFFFFFFFFFFFF00;
    }
    mul64(instrReg.X, regVal(instrReg.Y), instrReg.Z);
}

static void
imulqReg()
{
    mul64(regVal(instrReg.X), regVal(instrReg.Y), instrReg.Z);
}

static void
mulqImm()
{
    mul128(instrReg.X, regVal(instrReg.Y), instrReg.Z);
}

static void
mulqReg()
{
    mul128(regVal(instrReg.X), regVal(instrReg.Y), instrReg.Z);
}

static void
divqImm()
{
    div128(instrReg.X, regVal(instrReg.Y), regVal(instrReg.Y + 1), instrReg.Z);
}

static void
divqReg()
{
    div128(regVal(instrReg.X), regVal(instrReg.Y), regVal(instrReg.Y + 1),
	   instrReg.Z);
}

static void
idivqImm()
{
    idiv64(instrReg.X, regVal(instrReg.Y), instrReg.Z);
}

static void
idivqReg()
{
    idiv64(regVal(instrReg.X), regVal(instrReg.Y), instrReg.Z);
}

static void
shlqImm()
{
    shiftLeft64(instrReg.X, regVal(instrReg.Y), instrReg.Z);
}

static void
shlqReg()
{
    shiftLeft64(regVal(instrReg.X), regVal(instrReg.Y), instrReg.Z);
}

static void
sarqImm()
{
    shiftRightSigned64(instrReg.X, regVal(instrReg.Y), instrReg.Z);
}

static void
sarqReg()
{
    shiftRightSigned64(regVal(instrReg.X), regVal(instrReg.Y), instrReg.Z);
}

static void
shrqImm()
{
    shiftRightUnsigned64(instrReg.X, regVal(instrReg.Y), instrReg.Z);
}

static void
shrqReg()
{
    shiftRightUnsigned64(regVal(instrReg.X), regVal(instrReg.Y), instrReg.Z);
}

static void
andq()
{
    and64(regVal(instrReg.X), regVal(instrReg.Y), instrReg.Z);
}

static void
orq()
{
    or64(regVal(instrReg.X), regVal(instrReg.Y), instrReg.Z);
}

static void
notq()
{
    not64(regVal(instrReg.X), instrReg.Y);
}

static void
loadSigned()
{
    if (instrReg.XY <= 0x7FFF) {
	setReg(instrReg.XY, instrReg.Z);
    } else {
	setReg(0xFFFFFFFFFFFF0000 | instrReg.XY, instrReg.Z);
    }
}

static void
loadUnsigned()
{
    setReg(instrReg.XY, instrReg.Z);
}

static void
shiftLeftLoad()
{
    setReg(regVal(instrReg.Z) << 16 | instrReg.XY, instrReg.Z);
}

static void
addqImm()
{
    add64(instrReg.X, regVal(instrReg.Y), instrReg.Z);  
}

static void
addqReg()
{
    add64(regVal(instrReg.X), regVal(instrReg.Y), instrReg.Z);  
}

static void
subqImm()
{
    sub64(instrReg.X, regVal(instrReg.Y), instrReg.Z);  
}

static void
subqReg()
{
    sub64(regVal(instrReg.X), regVal(instrReg.Y), instrReg.Z);  
}

//- bus

//-- fetch

enum {
    ZERO_REG = 0,
    S1 = 1,
    S2 = 2,
    S4 = 4,
    S8 = 8,
    BYTE = 1,
    WORD = 2,
    LONG = 4,
    QUAD = 8,
};

//-- fetch byte

static void
fetchZByteDispl()
{
    fetch64(instrReg.X, instrReg.Y, ZERO_REG, S1, ZERO_EXT, BYTE, instrReg.Z);
}

static void
fetchZByteIndexS1()
{
    fetch64(0, instrReg.X, instrReg.Y, S1, ZERO_EXT, BYTE, instrReg.Z);
}

static void
fetchZByteIndexS2()
{
    fetch64(0, instrReg.X, instrReg.Y, S2, ZERO_EXT, BYTE, instrReg.Z);
}

static void
fetchZByteIndexS4()
{
    fetch64(0, instrReg.X, instrReg.Y, S4, ZERO_EXT, BYTE, instrReg.Z);
}

static void
fetchZByteIndexS8()
{
    fetch64(0, instrReg.X, instrReg.Y, S8, ZERO_EXT, BYTE, instrReg.Z);
}

static void
fetchSByteDispl()
{
    fetch64(instrReg.X, instrReg.Y, ZERO_REG, S1, SIGN_EXT, BYTE, instrReg.Z);
}

static void
fetchSByteIndexS1()
{
    fetch64(0, instrReg.X, instrReg.Y, S1, SIGN_EXT, BYTE, instrReg.Z);
}

static void
fetchSByteIndexS2()
{
    fetch64(0, instrReg.X, instrReg.Y, S2, SIGN_EXT, BYTE, instrReg.Z);
}

static void
fetchSByteIndexS4()
{
    fetch64(0, instrReg.X, instrReg.Y, S4, SIGN_EXT, BYTE, instrReg.Z);
}

static void
fetchSByteIndexS8()
{
    fetch64(0, instrReg.X, instrReg.Y, S8, SIGN_EXT, BYTE, instrReg.Z);
}

//-- fetch word

static void
fetchZWordDispl()
{
    fetch64(instrReg.X, instrReg.Y, ZERO_REG, S1, ZERO_EXT, WORD, instrReg.Z);
}

static void
fetchZWordIndexS1()
{
    fetch64(0, instrReg.X, instrReg.Y, S1, ZERO_EXT, WORD, instrReg.Z);
}

static void
fetchZWordIndexS2()
{
    fetch64(0, instrReg.X, instrReg.Y, S2, ZERO_EXT, WORD, instrReg.Z);
}

static void
fetchZWordIndexS4()
{
    fetch64(0, instrReg.X, instrReg.Y, S4, ZERO_EXT, WORD, instrReg.Z);
}

static void
fetchZWordIndexS8()
{
    fetch64(0, instrReg.X, instrReg.Y, S8, ZERO_EXT, WORD, instrReg.Z);
}

static void
fetchSWordDispl()
{
    fetch64(instrReg.X, instrReg.Y, ZERO_REG, S1, SIGN_EXT, WORD, instrReg.Z);
}

static void
fetchSWordIndexS1()
{
    fetch64(0, instrReg.X, instrReg.Y, S1, SIGN_EXT, WORD, instrReg.Z);
}

static void
fetchSWordIndexS2()
{
    fetch64(0, instrReg.X, instrReg.Y, S2, SIGN_EXT, WORD, instrReg.Z);
}

static void
fetchSWordIndexS4()
{
    fetch64(0, instrReg.X, instrReg.Y, S4, SIGN_EXT, WORD, instrReg.Z);
}

static void
fetchSWordIndexS8()
{
    fetch64(0, instrReg.X, instrReg.Y, S8, SIGN_EXT, WORD, instrReg.Z);
}

//-- fetch long

static void
fetchZLongDispl()
{
    fetch64(instrReg.X, instrReg.Y, ZERO_REG, S1, ZERO_EXT, LONG, instrReg.Z);
}

static void
fetchZLongIndexS1()
{
    fetch64(0, instrReg.X, instrReg.Y, S1, ZERO_EXT, LONG, instrReg.Z);
}

static void
fetchZLongIndexS2()
{
    fetch64(0, instrReg.X, instrReg.Y, S2, ZERO_EXT, LONG, instrReg.Z);
}

static void
fetchZLongIndexS4()
{
    fetch64(0, instrReg.X, instrReg.Y, S4, ZERO_EXT, LONG, instrReg.Z);
}

static void
fetchZLongIndexS8()
{
    fetch64(0, instrReg.X, instrReg.Y, S8, ZERO_EXT, LONG, instrReg.Z);
}

static void
fetchSLongDispl()
{
    fetch64(instrReg.X, instrReg.Y, ZERO_REG, S1, SIGN_EXT, LONG, instrReg.Z);
}

static void
fetchSLongIndexS1()
{
    fetch64(0, instrReg.X, instrReg.Y, S1, SIGN_EXT, LONG, instrReg.Z);
}

static void
fetchSLongIndexS2()
{
    fetch64(0, instrReg.X, instrReg.Y, S2, SIGN_EXT, LONG, instrReg.Z);
}

static void
fetchSLongIndexS4()
{
    fetch64(0, instrReg.X, instrReg.Y, S4, SIGN_EXT, LONG, instrReg.Z);
}

static void
fetchSLongIndexS8()
{
    fetch64(0, instrReg.X, instrReg.Y, S8, SIGN_EXT, LONG, instrReg.Z);
}

//-- fetch quad

static void
fetchQuadDispl()
{
    fetch64(instrReg.X, instrReg.Y, ZERO_REG, S1, SIGN_EXT, QUAD, instrReg.Z);
}

static void
fetchQuadIndexS1()
{
    fetch64(0, instrReg.X, instrReg.Y, S1, ZERO_EXT, QUAD, instrReg.Z);
}

static void
fetchQuadIndexS2()
{
    fetch64(0, instrReg.X, instrReg.Y, S2, ZERO_EXT, QUAD, instrReg.Z);
}

static void
fetchQuadIndexS4()
{
    fetch64(0, instrReg.X, instrReg.Y, S4, ZERO_EXT, QUAD, instrReg.Z);
}

static void
fetchQuadIndexS8()
{
    fetch64(0, instrReg.X, instrReg.Y, S8, ZERO_EXT, QUAD, instrReg.Z);
}

//-- store

//-- store byte

static void
storeByteDispl()
{
    store64(instrReg.Y, instrReg.Z, ZERO_REG, S1, BYTE, instrReg.X);
}

static void
storeByteIndexS1()
{
    store64(0, instrReg.Y, instrReg.Z, S1, BYTE, instrReg.X);
}

static void
storeByteIndexS2()
{
    store64(0, instrReg.Y, instrReg.Z, S2, BYTE, instrReg.X);
}

static void
storeByteIndexS4()
{
    store64(0, instrReg.Y, instrReg.Z, S4, BYTE, instrReg.X);
}

static void
storeByteIndexS8()
{
    store64(0, instrReg.Y, instrReg.Z, S8, BYTE, instrReg.X);
}

//-- store word

static void
storeWordDispl()
{
    store64(instrReg.Y, instrReg.Z, ZERO_REG, S1, WORD, instrReg.X);
}

static void
storeWordIndexS1()
{
    store64(0, instrReg.Y, instrReg.Z, S1, WORD, instrReg.X);
}

static void
storeWordIndexS2()
{
    store64(0, instrReg.Y, instrReg.Z, S2, WORD, instrReg.X);
}

static void
storeWordIndexS4()
{
    store64(0, instrReg.Y, instrReg.Z, S4, WORD, instrReg.X);
}

static void
storeWordIndexS8()
{
    store64(0, instrReg.Y, instrReg.Z, S8, WORD, instrReg.X);
}

//-- store long

static void
storeLongDispl()
{
    store64(instrReg.Y, instrReg.Z, ZERO_REG, S1, LONG, instrReg.X);
}

static void
storeLongIndexS1()
{
    store64(0, instrReg.Y, instrReg.Z, S1, LONG, instrReg.X);
}

static void
storeLongIndexS2()
{
    store64(0, instrReg.Y, instrReg.Z, S2, LONG, instrReg.X);
}

static void
storeLongIndexS4()
{
    store64(0, instrReg.Y, instrReg.Z, S4, LONG, instrReg.X);
}

static void
storeLongIndexS8()
{
    store64(0, instrReg.Y, instrReg.Z, S8, LONG, instrReg.X);
}

//-- store quad

static void
storeQuadDispl()
{
    store64(instrReg.Y, instrReg.Z, ZERO_REG, S1, QUAD, instrReg.X);
}

static void
storeQuadIndexS1()
{
    store64(0, instrReg.Y, instrReg.Z, S1, QUAD, instrReg.X);
}

static void
storeQuadIndexS2()
{
    store64(0, instrReg.Y, instrReg.Z, S2, QUAD, instrReg.X);
}

static void
storeQuadIndexS4()
{
    store64(0, instrReg.Y, instrReg.Z, S4, QUAD, instrReg.X);
}

static void
storeQuadIndexS8()
{
    store64(0, instrReg.Y, instrReg.Z, S8, QUAD, instrReg.X);
}

//------------------------------------------------------------------------------

struct OpCode opCode[256] = {
    // 0x00
    {0, 0},
    // 0x.1
    {haltReg, 0},
    // 0x.2
    {trap, 0},
    // 0x.3
    {0, 0},
    // 0x.4
    {0, 0},
    // 0x.5
    {0, 0},
    // 0x.6
    {0, 0},
    // 0x.7
    {0, 0},
    // 0x.8
    {0, 0},
    // 0x.9
    {haltImm, 0},
    // 0x.A
    {0, 0},
    // 0x.B
    {0, 0},
    // 0x.C
    {0, 0},
    // 0x.D
    {0, 0},
    // 0x.E
    {0, 0},
    // 0x.f
    {0, 0},

    // 0x10
    {fetchQuadIndexS1, 0},
    // 0x11
    {fetchZLongIndexS1, 0},
    // 0x12
    {fetchZWordIndexS1, 0},
    // 0x13
    {fetchZByteIndexS1, 0},
    // 0x14
    {0, 0},
    // 0x15
    {fetchSLongIndexS1, 0},
    // 0x16
    {fetchSWordIndexS1, 0},
    // 0x17
    {fetchSByteIndexS1, 0},
    // 0x18
    {fetchQuadDispl, 0},
    // 0x19
    {fetchZLongDispl, 0},
    // 0x1A
    {fetchZWordDispl, 0},
    // 0x1B
    {fetchZByteDispl, 0},
    // 0x1C
    {0, 0},
    // 0x1D
    {fetchSLongDispl, 0},
    // 0x1E
    {fetchSWordDispl, 0},
    // 0x1F
    {fetchSByteDispl, 0},

    // 0x20
    {storeQuadIndexS1, 0},
    // 0x21
    {storeLongIndexS1, 0},
    // 0x22
    {storeWordIndexS1, 0},
    // 0x23
    {storeByteIndexS1, 0},
    // 0x24
    {0, 0},
    // 0x25
    {0, 0},
    // 0x26
    {0, 0},
    // 0x27
    {0, 0},
    // 0x28
    {storeQuadDispl, 0},
    // 0x29
    {storeLongDispl, 0},
    // 0x2A
    {storeWordDispl, 0},
    // 0x2B
    {storeByteDispl, 0},
    // 0x2C
    {0, 0},
    // 0x2D
    {0, 0},
    // 0x2E
    {0, 0},
    // 0x2F
    {0, 0},

    // 0x30
    {addqReg, 0},
    // 0x31
    {subqReg, 0},
    // 0x32
    {mulqReg, 0},
    // 0x33
    {divqReg, 0},
    // 0x34
    {imulqReg, 0},
    // 0x35
    {idivqReg, 0},
    // 0x36
    {0, 0},
    // 0x37
    {0, 0},
    // 0x38
    {addqImm, 0},
    // 0x39
    {subqImm, 0},
    // 0x3A
    {mulqImm, 0},
    // 0x3B
    {divqImm, 0},
    // 0x3C
    {imulqImm, 0},
    // 0x3D
    {idivqImm, 0},
    // 0x3E
    {0, 0},
    // 0x3F
    {0, 0},

    // 0x40
    {jmpReg, 0},
    // 0x41
    {jmpRel, 0},
    // 0x42
    {je, 0},
    // 0x43
    {jne, 0},
    // 0x44
    {jnge, 0},
    // 0x45
    {jge, 0},
    // 0x46
    {jle, 0},
    // 0x47
    {jg, 0},
    // 0x48
    {jnae, 0},
    // 0x49
    {jae, 0},
    // 0x4A
    {jna, 0},
    // 0x4B
    {ja, 0},
    // 0x4C
    {0, 0},
    // 0x4D
    {0, 0},
    // 0x4E
    {0, 0},
    // 0x4F
    {0, 0},

    // 0x50
    {orq, 0},
    // 0x51
    {andq, 0},
    // 0x52
    {shlqReg, 0},
    // 0x53
    {shrqReg, 0},
    // 0x54
    {sarqReg, 0},
    // 0x55
    {0, 0},
    // 0x56
    {loadUnsigned, 0},
    // 0x57
    {loadSigned, 0},
    // 0x58
    {0, 0},
    // 0x59
    {0, 0},
    // 0x5A
    {shlqImm, 0},
    // 0x5B
    {shrqImm, 0},
    // 0x5C
    {sarqImm, 0},
    // 0x5D
    {shiftLeftLoad, 0},
    // 0x5E
    {notq, 0},
    // 0x5f
    {0, 0},

    // 0x60
    {getcReg, 0},
    // 0x61
    {putcReg, 0},
    // 0x62
    {0, 0},
    // 0x63
    {0, 0},
    // 0x64
    {0, 0},
    // 0x65
    {0, 0},
    // 0x66
    {0, 0},
    // 0x67
    {0, 0},
    // 0x68
    {0, 0},
    // 0x69
    {putcImm, 0},
    // 0x6A
    {0, 0},
    // 0x6B
    {0, 0},
    // 0x6C
    {0, 0},
    // 0x6D
    {0, 0},
    // 0x6E
    {0, 0},
    // 0x6F
    {0, 0},

    // 0x70
    {0, 0},
    // 0x.1
    {0, 0},
    // 0x.2
    {0, 0},
    // 0x.3
    {0, 0},
    // 0x.4
    {0, 0},
    // 0x.5
    {0, 0},
    // 0x.6
    {0, 0},
    // 0x.7
    {0, 0},
    // 0x.8
    {0, 0},
    // 0x.9
    {0, 0},
    // 0x.A
    {0, 0},
    // 0x.B
    {0, 0},
    // 0x.C
    {0, 0},
    // 0x.D
    {0, 0},
    // 0x.E
    {0, 0},
    // 0x.f
    {0, 0},

    // 0x80
    {fetchQuadIndexS2, 0},
    // 0x81
    {fetchZLongIndexS2, 0},
    // 0x82
    {fetchZWordIndexS2, 0},
    // 0x83
    {fetchZByteIndexS2, 0},
    // 0x84
    {0, 0},
    // 0x85
    {fetchSLongIndexS2, 0},
    // 0x86
    {fetchSWordIndexS2, 0},
    // 0x87
    {fetchSByteIndexS2, 0},
    // 0x88
    {0, 0},
    // 0x89
    {0, 0},
    // 0x8A
    {0, 0},
    // 0x8B
    {0, 0},
    // 0x8C
    {0, 0},
    // 0x8D
    {0, 0},
    // 0x8E
    {0, 0},
    // 0x8F
    {0, 0},

    // 0x90
    {storeQuadIndexS2, 0},
    // 0x.1
    {storeLongIndexS2, 0},
    // 0x.2
    {storeWordIndexS2, 0},
    // 0x.3
    {storeByteIndexS2, 0},
    // 0x.4
    {0, 0},
    // 0x.5
    {0, 0},
    // 0x.6
    {0, 0},
    // 0x.7
    {0, 0},
    // 0x.8
    {0, 0},
    // 0x.9
    {0, 0},
    // 0x.A
    {0, 0},
    // 0x.B
    {0, 0},
    // 0x.C
    {0, 0},
    // 0x.D
    {0, 0},
    // 0x.E
    {0, 0},
    // 0x.F
    {0, 0},

    // 0xA0
    {fetchQuadIndexS4, 0},
    // 0xA1
    {fetchZLongIndexS4, 0},
    // 0xA2
    {fetchZWordIndexS4, 0},
    // 0xA3
    {fetchZByteIndexS4, 0},
    // 0xA4
    {0, 0},
    // 0xA5
    {fetchSLongIndexS4, 0},
    // 0xA6
    {fetchSWordIndexS4, 0},
    // 0xA7
    {fetchSByteIndexS4, 0},
    // 0xA8
    {0, 0},
    // 0xA9
    {0, 0},
    // 0xAA
    {0, 0},
    // 0xAB
    {0, 0},
    // 0xAC
    {0, 0},
    // 0xAD
    {0, 0},
    // 0xAE
    {0, 0},
    // 0xAF
    {0, 0},

    // 0xB0
    {storeQuadIndexS4, 0},
    // 0xB1
    {storeLongIndexS4, 0},
    // 0xB2
    {storeWordIndexS4, 0},
    // 0xB3
    {storeByteIndexS4, 0},
    // 0xB4
    {0, 0},
    // 0xB5
    {0, 0},
    // 0xB6
    {0, 0},
    // 0xB7
    {0, 0},
    // 0xB8
    {0, 0},
    // 0xB9
    {0, 0},
    // 0xBA
    {0, 0},
    // 0xBB
    {0, 0},
    // 0xBC
    {0, 0},
    // 0xBD
    {0, 0},
    // 0xBE
    {0, 0},
    // 0xBF
    {0, 0},

    // 0xC0
    {fetchQuadIndexS8, 0},
    // 0xC1
    {fetchZLongIndexS8, 0},
    // 0xC2
    {fetchZWordIndexS8, 0},
    // 0xC3
    {fetchZByteIndexS8, 0},
    // 0xC4
    {0, 0},
    // 0xC5
    {fetchSLongIndexS8, 0},
    // 0xC6
    {fetchSWordIndexS8, 0},
    // 0xC7
    {fetchSByteIndexS8, 0},
    // 0xC8
    {0, 0},
    // 0xC9
    {0, 0},
    // 0xCA
    {0, 0},
    // 0xCB
    {0, 0},
    // 0xCC
    {0, 0},
    // 0xCD
    {0, 0},
    // 0xCE
    {0, 0},
    // 0xCF
    {0, 0},

    // 0xD0
    {storeQuadIndexS8, 0},
    // 0xD1
    {storeLongIndexS8, 0},
    // 0xD2
    {storeWordIndexS8, 0},
    // 0xD3
    {storeByteIndexS8, 0},
    // 0xD4
    {0, 0},
    // 0xD5
    {0, 0},
    // 0xD6
    {0, 0},
    // 0xD7
    {0, 0},
    // 0xD8
    {0, 0},
    // 0xD9
    {0, 0},
    // 0xDA
    {0, 0},
    // 0xDB
    {0, 0},
    // 0xDC
    {0, 0},
    // 0xDD
    {0, 0},
    // 0xDE
    {0, 0},
    // 0xDF
    {0, 0},

    // 0xE0
    {0, 0},
    // 0x.1
    {0, 0},
    // 0x.2
    {0, 0},
    // 0x.3
    {0, 0},
    // 0x.4
    {0, 0},
    // 0x.5
    {0, 0},
    // 0x.6
    {0, 0},
    // 0x.7
    {0, 0},
    // 0x.8
    {0, 0},
    // 0x.9
    {0, 0},
    // 0x.A
    {0, 0},
    // 0x.B
    {0, 0},
    // 0x.C
    {0, 0},
    // 0x.D
    {0, 0},
    // 0x.E
    {0, 0},
    // 0x.f
    {0, 0},

    // 0xF0
    {0, 0},
    // 0xF1
    {0, 0},
    // 0xF2
    {0, 0},
    // 0xF3
    {0, 0},
    // 0xF4
    {0, 0},
    // 0xF5
    {0, 0},
    // 0xF6
    {0, 0},
    // 0xF7
    {0, 0},
    // 0xF8
    {0, 0},
    // 0xF9
    {0, 0},
    // 0xFA
    {0, 0},
    // 0xFB
    {0, 0},
    // 0xFC
    {0, 0},
    // 0xFD
    {0, 0},
    // 0xFE
    {0, 0},
    // 0xFf
    {nop, 0},
};



