#ifndef ULM_CU_H
#define ULM_CU_H

#include <stdint.h>

#include "alu.h"

extern union InstrReg {
    uint32_t OpXYZ;
    struct {
	uint32_t Z : 8;
	uint32_t Y : 8;
	uint32_t X : 8;
	uint32_t Op : 8;
    };
    struct {
	uint32_t : 8;
	uint32_t XY : 16;
	uint32_t : 8;
    };
    struct {
	uint32_t XYZ : 24;
	uint32_t : 8;
    };
} instrReg;

void absJump(uint64_t addr, Reg retReg);
void relJump(uint64_t offset);
void halt(int8_t code);

void fetchInstr();
void decodeInstr();
void executeInstr();
void incrInstrPtr();

void run();

void printCU();

#endif // ULM_CU_H
